/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define XPRO_SPI_SCK_Pin GPIO_PIN_2
#define XPRO_SPI_SCK_GPIO_Port GPIOE
#define XPRO_SS_Pin GPIO_PIN_4
#define XPRO_SS_GPIO_Port GPIOE
#define XPRO_MISO_Pin GPIO_PIN_5
#define XPRO_MISO_GPIO_Port GPIOE
#define XPRO_MOSI_Pin GPIO_PIN_6
#define XPRO_MOSI_GPIO_Port GPIOE
#define USER_Btn_Pin GPIO_PIN_13
#define USER_Btn_GPIO_Port GPIOC
#define USER_Btn_EXTI_IRQn EXTI15_10_IRQn
#define XPRO_SDA_Pin GPIO_PIN_0
#define XPRO_SDA_GPIO_Port GPIOF
#define XPRO_SCL_Pin GPIO_PIN_1
#define XPRO_SCL_GPIO_Port GPIOF
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOH
#define RMII_MDC_Pin GPIO_PIN_1
#define RMII_MDC_GPIO_Port GPIOC
#define RMII_REF_CLK_Pin GPIO_PIN_1
#define RMII_REF_CLK_GPIO_Port GPIOA
#define RMII_MDIO_Pin GPIO_PIN_2
#define RMII_MDIO_GPIO_Port GPIOA
#define RMII_CRS_DV_Pin GPIO_PIN_7
#define RMII_CRS_DV_GPIO_Port GPIOA
#define RMII_RXD0_Pin GPIO_PIN_4
#define RMII_RXD0_GPIO_Port GPIOC
#define RMII_RXD1_Pin GPIO_PIN_5
#define RMII_RXD1_GPIO_Port GPIOC
#define Mixer_RST_Pin GPIO_PIN_11
#define Mixer_RST_GPIO_Port GPIOF
#define Mixer_ENX_Pin GPIO_PIN_12
#define Mixer_ENX_GPIO_Port GPIOF
#define FM_Pin GPIO_PIN_13
#define FM_GPIO_Port GPIOF
#define Mixer_SCLK_Pin GPIO_PIN_14
#define Mixer_SCLK_GPIO_Port GPIOF
#define Mixer_SDA_Pin GPIO_PIN_15
#define Mixer_SDA_GPIO_Port GPIOF
#define XPRO_IRQ_Pin GPIO_PIN_12
#define XPRO_IRQ_GPIO_Port GPIOE
#define XPRO_IRQ_EXTI_IRQn EXTI15_10_IRQn
#define XPRO_LED_D1_G_Pin GPIO_PIN_13
#define XPRO_LED_D1_G_GPIO_Port GPIOE
#define XPRO_Reset_Pin GPIO_PIN_14
#define XPRO_Reset_GPIO_Port GPIOE
#define XPRO_LED_D2_Y_Pin GPIO_PIN_15
#define XPRO_LED_D2_Y_GPIO_Port GPIOE
#define RMII_TXD1_Pin GPIO_PIN_13
#define RMII_TXD1_GPIO_Port GPIOB
#define LD3_Pin GPIO_PIN_14
#define LD3_GPIO_Port GPIOB
#define STLK_RX_Pin GPIO_PIN_8
#define STLK_RX_GPIO_Port GPIOD
#define STLK_TX_Pin GPIO_PIN_9
#define STLK_TX_GPIO_Port GPIOD
#define LOCK_DO_Pin GPIO_PIN_10
#define LOCK_DO_GPIO_Port GPIOD
#define RFSW_VCTL_Pin GPIO_PIN_4
#define RFSW_VCTL_GPIO_Port GPIOG
#define RFSW_EN_Pin GPIO_PIN_5
#define RFSW_EN_GPIO_Port GPIOG
#define USB_PowerSwitchOn_Pin GPIO_PIN_6
#define USB_PowerSwitchOn_GPIO_Port GPIOG
#define USB_OverCurrent_Pin GPIO_PIN_7
#define USB_OverCurrent_GPIO_Port GPIOG
#define USB_SOF_Pin GPIO_PIN_8
#define USB_SOF_GPIO_Port GPIOA
#define USB_VBUS_Pin GPIO_PIN_9
#define USB_VBUS_GPIO_Port GPIOA
#define USB_ID_Pin GPIO_PIN_10
#define USB_ID_GPIO_Port GPIOA
#define USB_DM_Pin GPIO_PIN_11
#define USB_DM_GPIO_Port GPIOA
#define USB_DP_Pin GPIO_PIN_12
#define USB_DP_GPIO_Port GPIOA
#define FPGA_SPI_SCK_Pin GPIO_PIN_10
#define FPGA_SPI_SCK_GPIO_Port GPIOC
#define FPGA_SPI_MISO_Pin GPIO_PIN_11
#define FPGA_SPI_MISO_GPIO_Port GPIOC
#define FPGA_SPI_MOSI_Pin GPIO_PIN_12
#define FPGA_SPI_MOSI_GPIO_Port GPIOC
#define FPGA_SPI_SEL_Pin GPIO_PIN_2
#define FPGA_SPI_SEL_GPIO_Port GPIOD
#define RMII_TX_EN_Pin GPIO_PIN_11
#define RMII_TX_EN_GPIO_Port GPIOG
#define RMII_TXD0_Pin GPIO_PIN_13
#define RMII_TXD0_GPIO_Port GPIOG
#define Mixer_MODE_Pin GPIO_PIN_14
#define Mixer_MODE_GPIO_Port GPIOG
#define LD2_Pin GPIO_PIN_7
#define LD2_GPIO_Port GPIOB
#define Mixer_EN_Pin GPIO_PIN_0
#define Mixer_EN_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
